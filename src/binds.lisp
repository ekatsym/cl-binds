(in-package :binds)

(defmacro cons-bind ((car-var cdr-var) cons &body body)
  (with-gensyms (variable)
    `(let ((,variable ,cons))
       (let ((,car-var (car ,variable))
             (,cdr-var (cdr ,variable)))
         ,@body))))

(defmacro list-bind (list-vars list &body body)
  `(destructuring-bind ,list-vars ,list ,@body))

(defmacro vector-bind (vector-vars vector &body body)
  (with-gensyms (variable)
    `(let ((,variable ,vector))
       (let (,@(loop for i below (length vector)
                     for var in vector-vars
                     collect `(,var (svref ,variable ,i))))
         ,@body))))

(defmacro array-bind (array-bindings array &body body)
  (with-gensyms (variable)
    `(let ((,variable ,array))
       (let (,@(mapcar (lambda (array-binding)
                         (list-bind (array-var &rest subscripts) array-binding
                           `(,array-var (aref ,variable ,@subscripts))))
                       array-bindings))
         ,@body))))

(defmacro hash-table-bind (key-bindings hash-table &body body)
  (with-gensyms (variable)
    `(let ((,variable ,hash-table))
       (let (,@(mapcar (lambda (key-binding)
                         (list-bind (key-var key-name) key-binding)
                         `(,key-var (gethash ',key-name ,variable)))
                       key-bindings))
         ,@body))))

(defmacro slot-bind (slot-bindings object &body body)
  (with-gensyms (variable)
    `(let ((,variable ,object))
       (let (,@(mapcar (lambda (slot-binding)
                         (list-bind (slot-var slot-name) slot-binding
                           `(,slot-var (slot-value ,variable ',slot-name))))
                       slot-bindings))
         ,@body))))
