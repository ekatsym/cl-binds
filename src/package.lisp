(in-package :common-lisp)

(defpackage cl-binds
  (:nicknames :binds)
  (:use :cl)
  (:import-from :alexandria
                #:with-gensyms
                )
  (:export #:cons-bind
           #:list-bind
           #:vector-bind
           #:array-bind
           #:hash-table-bind
           #:slot-bind
           )
  )
