(defsystem cl-binds
  :author "Hirotetsu Hongo <ekatsymeee@gmail.com>"
  :maintainer "Hirotetsu Hongo <ekatsym@gmail.com>"
  :license "MIT"
  :version "0.0.1"
  :depends-on (:alexandria)
  :components ((:module "src"
                :serial t
                :components
                ((:file "package")
                 (:file "binds")))))
